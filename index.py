from flask import Flask, render_template, request
import pickle
import numpy as np
import pandas as pd

app = Flask(__name__)


@app.route('/')
def home():
	return render_template('Home.html')

@app.route('/about')
def about():
	return render_template('About.html')

@app.route('/movie_results', methods=['POST'])
def movie():
	return render_template ('movie_results.html')

if __name__ == '__main__':
	app.run(debug=True)

