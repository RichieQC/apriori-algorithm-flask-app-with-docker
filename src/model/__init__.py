import numpy as np
from apyori import apriori
import pandas as pd

from joblib import dump, load
from matplotlib import pyplot as plt

import io
import base64
import pprint
import string



def get_rules(support, popular):
    records = load("records.joblib")
    association_rules = apriori(records, min_support=supportport, min_confidence=popular, min_lift=3, min_length=2)
    return list(association_rules)     

def get_movie_relations(movie, support, popular,n_recomendations):
    support = support/7501
    popular = popular/100
    association_rules = get_rules(support, popular, n_recomendations)
    mov = []
    for item in association_results:
        if len(item[0]) >= n_recomendations:
            rule = "Rule: "
            # first index of the inner list
            # Contains base item and add item
            pair = item[0] 
            items = [x for x in pair]
            if movie in items:
                flag = 0
                for i in items:
                    flag = flag + 1
                    if flag != len(items):
                        rule = rule + i + '->'
                    else:
                        rule = rule + i
                    if i != movie:
                        mov.append(i)
    mov = np.uniqnue(mov)

    return mov
    
    

    