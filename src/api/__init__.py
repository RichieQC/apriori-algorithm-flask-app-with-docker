from flask import Flask, jsonify, render_template, request
import pandas as pd
import json
import model

app = Flask(__name__)

@app.route("/", methods=["GET"])
def home():
    if request.method == "GET": 
        return render_template(
            "Home.html"
        )

@app.route("/About", methods=["POST"])
def about():
    if request.method == 'POST':
        return render_template(
            "About.html"
        )

@app.route("/movie_results", methods=["POST"])
def movie_relations():
    if request.method == "POST":
        return render_template(
            "movie_results.html"
        )


